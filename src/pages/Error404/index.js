import React from "react";
import PageDefault from "../../components/PageDefault";
import { Link } from "react-router-dom";

function Error404() {
  return (
    <PageDefault>
      <h1>404 Página Não Encontrada</h1>
      <h2>Ops, parece que esta página não existe!</h2>
      <Link to="/">Ir para Home</Link>
    </PageDefault>
  )
}

export default Error404;